import java.util.Scanner;

public class Lab2_exer7{
    public static void main(String[] args){
	int firstArray[] = new int[100];
	int secondArray[] = new int[100];
	int i, n;

	Scanner keyboard = new Scanner(System.in);
	
	System.out.printf("\nn = ");
	n = keyboard.nextInt();

	for (i = 0; i < n; i++){
	    System.out.printf("First array position %d: ", i+1);
	    firstArray[i] = keyboard.nextInt();
	}

	for (i = 0; i < n; i++){
	    System.out.printf("Second array position %d: ", i+1);
	    secondArray[i] = keyboard.nextInt();
	}


	System.out.printf("\nAdding Matrices: ");
	for (i = 0; i < n; i++)
	    System.out.printf("%d ", firstArray[i] + secondArray[i]); 
	System.out.printf("\n");
	
    }
}
