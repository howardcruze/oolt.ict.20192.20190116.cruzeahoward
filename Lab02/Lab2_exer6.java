import java.util.Scanner;

public class Lab2_exer6{
    public static void main(String[] args){
	int array[] = new int[100];
	int n;
	double sum = 0;
	int i, j, tmp;

	Scanner keyboard = new Scanner(System.in);

        System.out.printf("\nn = ");
	n = keyboard.nextInt();
	
	for (i = 0; i < n; i++){
	    System.out.printf("Position %d value: ", i+1);
	    array[i] = keyboard.nextInt();
	}
	
	for (i = 0; i < n; i++){
	    for (j = i; j < n; j++){
		if (array[j] < array[i]){
		    tmp = array[j];
		    array[j] = array[i];
		    array[i] = tmp;
		}
	    }
	}

	System.out.printf("\nSorted list: ");
	for (i = 0; i < n; i++){
	    if (i < n-1)
		System.out.printf("%d, ", array[i]);
	    else
		System.out.printf("%d.", array[i]);
	    sum += array[i];
	}
	
	System.out.printf("\nSum = %f \nAverage = %f\n", sum, sum/n);
    }
}
