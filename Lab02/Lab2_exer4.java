import java.util.Scanner;

public class Lab2_exer4{
    public static void main(String[] args){
	Scanner keyboard = new Scanner(System.in);

	System.out.printf("n: ");
	int n = keyboard.nextInt();
	int count = n - 1;

	for (int i = 0; i < n; i++){
	    if (i % 2 == 0){
		for (int j = 0; j < count; j++)
		    System.out.printf(" ");
		for (int j = 0; j < i+1; j++)
		    System.out.printf("*");
		System.out.println();
		count -= 1;
	    }
	    else
		n += 1;
	}

	
    }
}
