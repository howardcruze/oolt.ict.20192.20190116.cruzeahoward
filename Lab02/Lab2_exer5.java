import java.util.Scanner;

public class Lab2_exer5{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);

	int month = 0, year = 0, leap_year = 0;

	while (month < 1 || month > 12) {
	    System.out.printf("\nMonth (ex:03): ");
	    month = keyboard.nextInt();
	}
	
	while (year < 1) {
	    System.out.printf("\nYear (ex:2020): ");
	    year = keyboard.nextInt();
	}
	
	if (year % 4 == 0){
	    leap_year = 1;
	    if (year % 100 == 0){
		leap_year = 0;
		if (year % 400 == 0)
		    leap_year = 1;
	    }
	}

        if (month == 1 || month == 3 || month == 5 || month == 7 ||
	    month == 8 || month == 10 || month == 12)
	    System.out.printf("\n31 Days");

	else if (month == 2){
	    if (leap_year == 1)
		System.out.printf("\n29 Days");
	    else
		System.out.printf("\n28 Days");
	}

	else
	    System.out.printf("\n30 Days");

	System.out.printf("\n");
    }
}
