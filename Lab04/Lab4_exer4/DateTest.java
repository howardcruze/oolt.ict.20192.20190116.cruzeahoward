import java.util.Scanner;

public class DateTest {
    public static void main(String[] args) {

	int i;
	
	MyDate date1 = new MyDate();
	MyDate date2 = new MyDate();
	MyDate date3 = new MyDate();
	MyDate date4 = new MyDate();
	DateUtils utils = new DateUtils();
        Scanner keyboard = new Scanner(System.in);

	System.out.printf("Day (Ex: first): ");
	String str = keyboard.nextLine();
	date1.setDay(str);

	System.out.printf("Month (Ex: January): ");
	str = keyboard.nextLine();
	date1.setMonth(str);

	System.out.printf("Year (Ex: 2020): ");
	int x = keyboard.nextInt();
	date1.setYear(x);
	
	date1.print("dd/mm/yyyy");
	date1.print("yy-mm-dd");
	date1.print("string");

	date2.setDay(5);
	date2.setMonth(10);
	date2.setYear(2020);

	if (utils.compare(date1, date2) == 1)
	    System.out.printf("\n\nYour date is equal to %d/%d/%d\n", date2.getDay(), date2.getMonth(), date2.getYear());
	
	else
	    System.out.printf("\n\nYour date is not equal to %d/%d/%d\n", date2.getDay(), date2.getMonth(), date2.getYear());


	MyDate dates[] = new MyDate[4];
	dates[0] = date1;
	dates[1] = date2;
	
        date3.setDay(10);
	date3.setMonth(10);
        date3.setYear(2010);
	date4.setDay(2);
	date4.setMonth(2);
	date4.setYear(2002);

	dates[2] = date3;
	dates[3] = date4;
	
	dates = utils.sort(dates, 4);

	System.out.printf("\nSorted List");
	for (i = 0; i < 4; i++) {
	    dates[i].print("dd/mm/yyyy");
	}

	System.out.printf("\n");
	
    }
}
