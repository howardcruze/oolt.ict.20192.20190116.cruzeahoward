public class MyDate {
    private int day;
    private int month;
    private int year;

    private String dayStr;
    private String monthStr;

    public void print(String x) {
	if (x.equals("dd/mm/yyyy"))
	    System.out.printf("\n%d/%d/%d", day, month, year);
	else if (x.equals("yy-mm-dd"))
	    System.out.printf("\n%d-%d-%d", year % 100, month, day);
	else if (x.equals("string"))
	    System.out.printf("\n%s %s %d", monthStr, dayStr, year);
    }

    public void setYear(int x) {
	this.year = x;
    }

    public void setMonth(int x) {
	this.month = x;
    }

    public void setMonth(String x) {
	this.monthStr = x;
	
	if (x.equals("January"))
	    this.month = 1;

	else if (x.equals("February"))
	    this.month = 2;

	else if (x.equals("March"))
	    this.month = 3;

	else if (x.equals("April"))
	    this.month = 4;

	else if (x.equals("May"))
	    this.month = 5;

	else if (x.equals("June"))
	    this.month = 6;

	else if (x.equals("July"))
	    this.month = 7;

	else if (x.equals("August"))
	    this.month = 8;

	else if (x.equals("September"))
	    this.month = 9;

	else if (x.equals("October"))
	    this.month = 10;

	else if (x.equals("November"))
	    this.month = 11;

	else if (x.equals("December"))
	    this.month = 12;
    }
    
    public void setDay(int x) {
	this.day = x;
    }

    public void setDay(String x) {
	this.dayStr = x;
	
	if (x.equals("first"))
	    this.day = 1;
	
	else if (x.equals("second"))
	    this.day = 2;

	else if (x.equals("third"))
	    this.day = 3;

	else if (x.equals("fourth"))
	    this.day = 4;

	else if (x.equals("fifth"))
	    this.day = 5;

	else if (x.equals("sixth"))
	    this.day = 6;

	else if (x.equals("seventh"))
	    this.day = 7;

	else if (x.equals("eighth"))
	    this.day = 8;

	else if (x.equals("ninth"))
	    this.day = 9;

	else if (x.equals("tenth"))
	    this.day = 10;

	else if (x.equals("eleventh"))
	    this.day = 11;

	else if (x.equals("twelfth"))
	    this.day = 12;

	else if (x.equals("thirteenth"))
	    this.day = 13;

	else if (x.equals("fourteenth"))
	    this.day = 14;

	else if (x.equals("fifteenth"))
	    this.day = 15;

	else if (x.equals("sixteenth"))
	    this.day = 16;

	else if (x.equals("seventeenth"))
	    this.day = 17;

	else if (x.equals("eighteenth"))
	    this.day = 18;

	else if (x.equals("nineteenth"))
	    this.day = 19;

	else if (x.equals("twentieth"))
	    this.day = 20;

	else if (x.equals("twenty-first"))
	    this.day = 21;

	else if (x.equals("twenty-second"))
	    this.day = 22;

	else if (x.equals("twenty-third"))
	    this.day = 23;

	else if (x.equals("twenty-fourth"))
	    this.day = 24;

	else if (x.equals("twenty-fifth"))
	    this.day = 25;

	else if (x.equals("twenty-sixth"))
	    this.day = 26;

	else if (x.equals("twenty-seventh"))
	    this.day = 27;

	else if (x.equals("twenty-eighth"))
	    this.day = 28;

	else if (x.equals("twenty-ninth"))
	    this.day = 29;

	else if (x.equals("thirtieth"))
	    this.day = 30;

	else if (x.equals("thirty-first"))
	    this.day = 31;
    }

    

    public int getDay() { 
	return day;
    }

    public int getMonth() {
	return month;
    }

    public int getYear() {
	return year;
    }
}
