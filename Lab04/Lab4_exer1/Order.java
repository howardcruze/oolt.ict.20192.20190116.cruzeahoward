public class Order {
    
    public static final int MAX_NUMBERS_ORDERED = 10;
    public int order_count = 0;
    public int i;
    public double total_cost;

    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public void addDigitalVideoDisc(DigitalVideoDisc dvd) {
	this.itemsOrdered[order_count] = dvd;
	this.order_count += 1;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
	if (order_count + 2 > MAX_NUMBERS_ORDERED)
	    System.out.printf("Error: Too many orders");
	else {
	    this.itemsOrdered[order_count] = dvd1;
	    this.itemsOrdered[order_count + 1] =dvd2;
	    this.order_count += 2;
	}
	    
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvdList[], int n) {
	if (order_count + n > MAX_NUMBERS_ORDERED)
	    System.out.printf("Error: Too many orders");

	else
	    for (i = 0; i < n; i++) {
	        this.itemsOrdered[order_count] = dvdList[i];
		this.order_count += 1;
	    }
    }

    public double totalCost() {
	for (i = 0; i < order_count; i++)
	    total_cost += itemsOrdered[i].getCost();
	return total_cost;
    }
}
