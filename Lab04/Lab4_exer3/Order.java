public class Order {
    
    public static final int MAX_NUMBERS_ORDERED = 10;
    public int qtyOrdered = 0;
    public int i;
    public double total_cost;
    public int dateOrdered[] = new int[3];
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders = 0;
    
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public Order() {
	super();
	if (nbOrders >= MAX_LIMITED_ORDERS)
	    System.out.printf("Max number of orders reached");
	else
	    nbOrders += 1;
    }

    public void setDate(int day, int month, int year) {
	dateOrdered[0] = day;
	dateOrdered[1] = month;
	dateOrdered[2] = year;
    }

    public int []  getDate() {
	return dateOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd) {
	this.itemsOrdered[qtyOrdered] = dvd;
	this.qtyOrdered += 1;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
	if (qtyOrdered + 2 > MAX_NUMBERS_ORDERED)
	    System.out.printf("Error: Too many orders");
	else {
	    this.itemsOrdered[qtyOrdered] = dvd1;
	    this.itemsOrdered[qtyOrdered + 1] =dvd2;
	    this.qtyOrdered += 2;
	}
	    
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvdList[], int n) {
	if (qtyOrdered + n > MAX_NUMBERS_ORDERED)
	    System.out.printf("Error: Too many orders");

	else
	    for (i = 0; i < n; i++) {
	        this.itemsOrdered[qtyOrdered] = dvdList[i];
		this.qtyOrdered += 1;
	    }
    }

    public double totalCost() {
	for (i = 0; i < qtyOrdered; i++)
	    total_cost += itemsOrdered[i].getCost();
	return total_cost;
    }

    public void display() {
	System.out.printf("\n%d/%d/%d", dateOrdered[0], dateOrdered[1], dateOrdered[2]);
	for (i = 0; i < qtyOrdered; i++) {
	    System.out.printf("\n%d.DVD - %s - %s - %s - %s: $%.2f", i+1, itemsOrdered[i].getTitle(), itemsOrdered[i].getCategory(), itemsOrdered[i].getDirector(), itemsOrdered[i].getLength(), itemsOrdered[i].getCost());
	}
	System.out.printf("\nTotal cost is: ");
        System.out.printf("%.2f\n", this.totalCost());
    }
}
