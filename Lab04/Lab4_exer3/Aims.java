public class Aims{

    public static void main(String[] args){

	Order anOrder = new Order();
	anOrder.setDate(01, 12, 2019);

	DigitalVideoDisc dvd[] = new DigitalVideoDisc[10];

        dvd[0] = new DigitalVideoDisc("The Lion King");
	dvd[0].setCategory("Animation");
	dvd[0].setCost(19.95f);
	dvd[0].setDirector("Roger Allers");
	dvd[0].setLength(87);

        dvd[1] = new DigitalVideoDisc("Star Wars");
	dvd[1].setCategory("Science Fiction");
        dvd[1].setCost(24.95f);
        dvd[1].setDirector("George Lucas");
        dvd[1].setLength(124);
	
	dvd[2] = new DigitalVideoDisc("Aladdin");
	dvd[2].setCategory("Animation");
	dvd[2].setCost(18.99f);
	dvd[2].setDirector("John Musker");
	dvd[2].setLength(90);
	
	anOrder.addDigitalVideoDisc(dvd, 3);
	anOrder.display();

	Order anOrder2 = new Order();
	anOrder2.setDate(15, 06, 2020);

        dvd[0] = new DigitalVideoDisc("TestA");
	dvd[0].setCategory("Animation");
	dvd[0].setCost(25.00f);
	dvd[0].setDirector("TestA Director");
	dvd[0].setLength(90);

        dvd[1] = new DigitalVideoDisc("TestB");
	dvd[1].setCategory("Science Fiction");
        dvd[1].setCost(100.00f);
        dvd[1].setDirector("TestB Director");
        dvd[1].setLength(100);
	
	dvd[2] = new DigitalVideoDisc("TestC");
	dvd[2].setCategory("Romance");
	dvd[2].setCost(15.55f);
	dvd[2].setDirector("TestC Director");
	dvd[2].setLength(30);
	
	anOrder2.addDigitalVideoDisc(dvd, 3);
	anOrder2.display();
	
    }
}
