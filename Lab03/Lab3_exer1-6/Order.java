public class Order {
    
    public static final int MAX_NUMBERS_ORDERED = 10;
    public int order_count = 0;
    public int i;
    public double total_cost;

    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public void addDigitalVideoDisc(DigitalVideoDisc dvd) {
	this.itemsOrdered[order_count] = dvd;
	this.order_count += 1;
    }

    public double totalCost() {
	for (i = 0; i < order_count; i++)
	    total_cost += itemsOrdered[i].getCost();
	return total_cost;
    }
}
