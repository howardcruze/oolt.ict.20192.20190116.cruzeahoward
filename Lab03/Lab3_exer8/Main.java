public class Main {
    
    public static void main(String[] args) {

	MyDate date = new MyDate();
	
	date.setDay(21);
	date.setMonth(06);
	date.setYear(2016);
	System.out.printf("\n%d/%d/%d\n", date.getDay(), date.getMonth(), date.getYear());

	date.accept();
	date.print();

	date.setDay(31);
	date.setMonth(12);
	date.setYear(3000);
	date.print();
	
    }
}
