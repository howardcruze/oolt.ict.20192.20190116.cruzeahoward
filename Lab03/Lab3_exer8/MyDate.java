import java.util.Scanner;
    
public class MyDate {

    private Scanner keyboard = new Scanner(System.in);
    private int day, month, year;
    private String date;

    public void accept() {
	System.out.printf("\nEnter a year (0-inf): ");
	this.year = keyboard.nextInt();
	System.out.printf("\nEnter a month (1-12): ");
	this.month = keyboard.nextInt();
	System.out.printf("\nEnter a day (1-31): ");
	this.day = keyboard.nextInt();
    }

    public void print() {
	System.out.printf("\n%d/%d/%d\n", day, month, year);
    }

    public void setDay(int d) {
	if (d < 1 || d > 31)
	    System.out.printf("\nERROR: day value out of acceptable bounds\n");
	else
	    this.day = d;
    }

    public void setMonth(int m){
	if (m < 1 || m > 12)
	    System.out.printf("\nERROR: month value out of acceptable bounds\n");
	else
	    this.month = m;
    }

    public void setYear(int y){
	if (year < 0)
	    System.out.printf("\nERROR: year value out of acceptable bounds\n");
	else
	    this.year = y;
    }

    public int getDay(){
	return day;
    }
    
    public int getMonth(){
	return month;
    }

    public int getYear() {
	return year;
    }

}
