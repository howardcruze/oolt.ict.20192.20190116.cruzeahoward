public class Order {
    
    public static final int MAX_NUMBERS_ORDERED = 10;
    public int order_count = 0;
    public int i;
    public double total_cost;

    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    public DigitalVideoDisc blank_dvd = new DigitalVideoDisc("Blank");

    public void addDigitalVideoDisc(DigitalVideoDisc dvd) {
	if  (order_count < MAX_NUMBERS_ORDERED) {
	    this.itemsOrdered[order_count] = dvd;
	    this.order_count += 1;
	}

	else
	    for (i = 0; i < order_count; i++)
		if (itemsOrdered[i] == blank_dvd)
		    this.itemsOrdered[order_count] = dvd;
	
    }

    public double totalCost() {
	for (i = 0; i < order_count; i++)
	    total_cost += itemsOrdered[i].getCost();
	return total_cost;
    }

    public void remove(DigitalVideoDisc dvd){
	for (i = 0; i < order_count; i++)
	    if (itemsOrdered[i] == dvd)
	        itemsOrdered[i] = blank_dvd;
    }
}
