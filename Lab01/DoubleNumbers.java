import javax.swing.JOptionPane;

public class DoubleNumbers{
    public static void main(String[] args){
	String StrNum1, StrNum2;
	double num1, num2;

	StrNum1 = JOptionPane.showInputDialog(null, "Pleae input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
	num1 = Double.parseDouble(StrNum1);

	StrNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number:", JOptionPane.INFORMATION_MESSAGE);
	num2 = Double.parseDouble(StrNum2);

	JOptionPane.showMessageDialog(null, "Addition: " + (num1+num2) + "\nSubtraction: " + (num1-num2) + "\nMultiplication: " + (num1*num2) + "\nDivision: " + (num1/num2), "Operations", JOptionPane.INFORMATION_MESSAGE);
	System.exit(0);
    }
}
