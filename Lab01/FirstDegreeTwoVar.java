import javax.swing.JOptionPane;

public class FirstDegreeTwoVar{
    public static void main(String[] args){
	String strA, strB, strC;
	double a, b, c;

	strA = JOptionPane.showInputDialog(null, "A: ", "Input A", JOptionPane.INFORMATION_MESSAGE);
	a = Double.parseDouble(strA);

	strB = JOptionPane.showInputDialog(null, "B: ", "Input B", JOptionPane.INFORMATION_MESSAGE);
	b = Double.parseDouble(strB);

	strC = JOptionPane.showInputDialog(null, "C: ", "Input C", JOptionPane.INFORMATION_MESSAGE);
	c = Double.parseDouble(strC);

	JOptionPane.showMessageDialog(null, "y = " + ((-a)/b) + "x + " + ((-c)/b), "Result", JOptionPane.INFORMATION_MESSAGE);
	System.exit(0);
    }
}
