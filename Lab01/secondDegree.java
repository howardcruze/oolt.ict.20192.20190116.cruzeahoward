import javax.swing.JOptionPane;

public class secondDegree{
    public static void main(String[] args){
	String strA, strB, strC;
        double a, b, c, delta;

	strA = JOptionPane.showInputDialog(null, "A: ", "A's Value", JOptionPane.INFORMATION_MESSAGE);
	a = Double.parseDouble(strA);

	strB = JOptionPane.showInputDialog(null, "B: ", "B's Value", JOptionPane.INFORMATION_MESSAGE);
	b = Double.parseDouble(strB);

	strC = JOptionPane.showInputDialog(null, "C: ", "C's Value", JOptionPane.INFORMATION_MESSAGE);
	c = Double.parseDouble(strC);


	delta = b*b - 4*a*c;
	    
	if (delta > 0){
	    JOptionPane.showMessageDialog(null, "y1 = " +  ((-b + Math.sqrt(delta))/(2*a)) +  "\ny2 = " +  ((-b - Math.sqrt(delta))/(2*a)) , "Result", JOptionPane.INFORMATION_MESSAGE);
	}

	else if (delta == 0){
	    JOptionPane.showMessageDialog(null, "y = " +  (-b/(2*a)), "Result", JOptionPane.INFORMATION_MESSAGE);
	}

	else{
	    JOptionPane.showMessageDialog(null, "No Solution", "Result", JOptionPane.INFORMATION_MESSAGE);

	}
	    


    }
}
