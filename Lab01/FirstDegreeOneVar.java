import javax.swing.JOptionPane;

public class FirstDegreeOneVar{
    public static void main(String[] args){
	double a, b;
	String strA, strB;

	strA = JOptionPane.showInputDialog(null, "Please input a: ", "a's input", JOptionPane.INFORMATION_MESSAGE);
	a = Double.parseDouble(strA);

	strB = JOptionPane.showInputDialog(null, "Please input b: ", "b's input", JOptionPane.INFORMATION_MESSAGE);
	b = Double.parseDouble(strB);

	JOptionPane.showMessageDialog(null, "x = " + ((-b)/a), "Solution", JOptionPane.INFORMATION_MESSAGE);
    }
}
